<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OrderForm */
/* @var $form yii\widgets\ActiveForm */
?>

   
<h1><?= Yii::t('app', 'Calculate banana packages') ?></h1>

<div>
     <?php $form = ActiveForm::begin(); ?>
    
        <?= $form->field($model, 'quantity')->input('number') ?>
    
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Calculate'),  ['class' => 'btn btn-success']) ?>
        </div>
    
     <?php ActiveForm::end(); ?>
</div>

<?php if($model->packages) { ?>
<table class="table table-bordered">
    <thead>
        <tr>
            <th><?= Yii::t('app', 'Quantity') ?></th>
            <th><?= Yii::t('app', 'Package') ?></th>
        </tr>
    </thead>
    <tbody>
        
    <?php foreach ($model->packages as $package => $count) { ?>
        <tr>
            <td><?= $count ?></td>
            <td><?= $package ?></td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <th><?= Yii::t('app', 'Total') ?></th>
            <td><?= $model->total ?></td>
        </tr>
    </tfoot>
</table>
<?php } ?>
