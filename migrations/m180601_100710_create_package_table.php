<?php

use yii\db\Migration;

/**
 * Handles the creation of table `package`.
 */
class m180601_100710_create_package_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('package', [
            'id' => $this->primaryKey(),
            'count' => $this->integer(),
        ]);
        
        $this->batchInsert('package', ['count'], [
            [250],
            [500],
            [1000],
            [2000],
            [5000],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('package');
    }
}
