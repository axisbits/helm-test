<?php

namespace app\models;

use Yii;
use app\models\OrderItem;

/**
 * This is the model class for table "package".
 *
 * @property int $id
 * @property int $count
 *
 * @property OrderItem[] $orderItems
 */
class Package extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['count'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'count' => 'Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['package_id' => 'id']);
    }
    
    public function listAll()
    {
        return self::find()->select(['title', 'id'])->indexBy('id')->column();
    }
}
