<?php

namespace app\models;

use Yii;

class OrderForm extends \yii\base\Model
{
    public $quantity;
    protected $_packageList = [];
    protected $_packages = [];
    protected $_total = 0;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quantity'], 'required'],
            [['quantity'], 'integer', 'max' => 99999999999999999, 'min' => 1],
        ];
    }

    /**
     * Calculate packages
     * @return boolean
     */
    public function calculate()
    {
        return $this->calculatePackage($this->quantity);
    }

    /**
     * 
     * @param integer $quantity
     * @return boolean
     */
    protected function calculatePackage($quantity)
    {

        if (!($packList = $this->getPackageList()) || $quantity <= 0) {
            return false;
        }

        $remainder = $quantity;
        foreach ($packList as $pack) {
            $remainder = $this->processCalculatePackages($remainder, $pack);

            if ($remainder == 0) {
                return true;
            }
        }
        
        $this->clearPackages();
        return $this->calculatePackage($quantity - $remainder + $pack);
        
    }

    protected function processCalculatePackages($quantity, $pack)
    {
        if ($pack > $quantity) {
            return $quantity;
        }
        
        $countPackages = (int) ($quantity / $pack);
        $this->generatePackage($pack, $countPackages);
        
        return  $quantity % $pack;
    }

    /**
     * Reset packages and total
     * @param type $quantity
     */
    protected function clearPackages()
    {
        $this->setPackages([]);
        $this->setTotal(0);
    }

    /**
     * 
     * @param iteger $package
     * @param integer $quantity | default 1
     * @return $this
     */
    protected function generatePackage($package, $quantity = 1)
    {
        $this->addPackage($package, $quantity);
        $this->addTotal($package * $quantity);

        return $this;
    }

    /**
     * 
     * @param array $packages
     */
    public function setPackages(array $packages)
    {
        $this->_packages = $packages;
        return $this;
    }

    /**
     * 
     * @return array
     */
    public function getPackages()
    {
        return $this->_packages;
    }

    /**
     * 
     * @param integer $total
     * @return $this
     */
    public function setTotal($total)
    {
        $this->_total = $total;
        return $this;
    }

    /**
     * 
     * @return integer
     */
    public function getTotal()
    {
        return $this->_total;
    }

    /**
     * 
     * @param integer $package
     * @return $this
     */
    public function addTotal($package)
    {
        $this->_total += $package;
        return $this;
    }

    /**
     * 
     * @param integer $package
     * @param integer $quantity
     * @return $this
     */
    public function addPackage($package, $quantity)
    {
        $this->_packages[$package] = ($this->_packages[$package] ?? 0) + $quantity;

        return $this;
    }

    public function getPackageList()
    {
        if (!$this->_packageList) {
            $this->_packageList = Package::find()
                ->select(['count', 'id'])
                ->indexBy('id')
                ->orderBy(['count' => SORT_DESC])
                ->column();
        }

        return $this->_packageList;
    }

}
